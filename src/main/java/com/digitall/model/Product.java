package com.digitall.model;

public class Product {
    private String name;
    private String manufacturer;
    private int rating;
    private double price;
    private ProductType productType;

    public Product(String name, String manufacturer, int rating, double price, ProductType productType) {
        this.name = name;
        this.manufacturer = manufacturer;
        this.rating = rating;
        this.price = price;
        this.productType = productType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }
}
