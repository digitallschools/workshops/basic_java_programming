package com.digitall.model;

import java.util.*;


public class ProductReporter {

    List<Product> productList;

    public ProductReporter(List<Product> productList) {
        this.productList = productList;
    }

    public Map<ProductType, Integer> categorizeProductCountsByType() {
        return Collections.emptyMap();
    }

    // filter
    public List<Product> getProductsOfType(ProductType productType) {
        return Collections.emptyList();
    }


    //
    public Product getProductWithHighestRating() {
        return null;
    }


    public Product getProductWithLowestRating() {
        return null;
    }


    // should return min price in double[0] and max price in double[1]
    public double[] getProductPriceRange() {
        return new double[0]; // this  is empty initialization , we can use null as well
    }

    public Set<String> getUniqueManufacturerNames() {
      return Collections.emptySet();
    }

}
