package com.digitall.model;

public enum ProductType {
    BOOK, ELECTRONICS, APPAREL
}
