
package com.example.project;

import com.digitall.model.Product;
import com.digitall.model.ProductReporter;
import com.digitall.model.ProductType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import org.hamcrest.collection.IsMapContaining;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;


class ProductReporterTests {

    List<Product> productList;

    @BeforeEach
    void init() {
        productList = Arrays.asList(
                new Product("Max", "samsung", 4, 10000, ProductType.ELECTRONICS),
                new Product("Galaxy", "samsung", 3, 14000, ProductType.ELECTRONICS),
                new Product("Thunder", "oppo", 4, 15000, ProductType.ELECTRONICS),
                new Product("IPhone-10", "Apple", 2, 17000, ProductType.ELECTRONICS),
                new Product("sky", "redme", 3, 11000, ProductType.ELECTRONICS),
                new Product("Java In Action", "Manning", 3, 500, ProductType.BOOK),
                new Product("Spring In Depth", "Manning", 4, 640, ProductType.BOOK),
                new Product("Coat", "raymonds", 5, 6000, ProductType.APPAREL),
                new Product("Shirt", "raymonds", 4, 2000, ProductType.APPAREL),
                new Product("Pro Angular", "Manning", 4, 700, ProductType.BOOK),
                new Product("Shirt", "colorplus", 3, 900, ProductType.APPAREL)

        );

    }

    @Test
    void shouldCategorizeProductCountsByType() {

        // given
        ProductReporter productReporter = new ProductReporter(productList);

        // when
        Map<ProductType, Integer> productCountsByType = productReporter.categorizeProductCountsByType();

        // then
        assertNotNull(productCountsByType);
        assertEquals(3, productCountsByType.size());
        assertEquals(productCountsByType.get(ProductType.ELECTRONICS), 5);
        assertEquals(productCountsByType.get(ProductType.BOOK), 3);
        assertEquals(productCountsByType.get(ProductType.APPAREL), 3);
    }

    @Test
    void shouldCalculateProductWithHighestRating() {

        // given
        ProductReporter productReporter = new ProductReporter(productList);

        // when
        Product product = productReporter.getProductWithHighestRating();

        // then
        assertNotNull(product);
        assertEquals("raymonds", product.getManufacturer());
        assertEquals("Coat", product.getName());

    }

    @Test
    void shouldCalculatePriceRanger() {

        // given
        ProductReporter productReporter = new ProductReporter(productList);

        // when
        double[] priceRanges = productReporter.getProductPriceRange();

        // then
        assertNotNull(priceRanges);
        assertEquals(2, priceRanges.length);
        assertEquals(500, priceRanges[0]);
        assertEquals(17000, priceRanges[1]);
    }

    @Test
    void shouldCalculateProductWithLowestRating() {

        // given
        ProductReporter productReporter = new ProductReporter(productList);

        // when
        Product product = productReporter.getProductWithLowestRating();

        // then
        assertNotNull(product);
        assertEquals("Apple", product.getManufacturer());
        assertEquals("IPhone-10", product.getName());
        assertEquals(17000, product.getPrice());

    }

    @Test
    void shouldFilterByProductType() {

        // given
        ProductReporter productReporter = new ProductReporter(productList);

        // when
        List<Product> electronicProducts = productReporter.getProductsOfType(ProductType.ELECTRONICS);
        List<Product> apparelProducts = productReporter.getProductsOfType(ProductType.APPAREL);

        Product redMe = electronicProducts.stream()
                .filter( x -> x.getManufacturer().equals("redme"))
                .findFirst()
                .orElse(null);

        Product colorplus = apparelProducts.stream()
                .filter( x -> x.getManufacturer().equals("colorplus"))
                .findFirst()
                .orElse(null);

        // then
        assertNotNull(electronicProducts);
        assertEquals(5, electronicProducts.size());


        assertNotNull(apparelProducts);
        assertEquals(3, apparelProducts.size());



        assertNotNull(redMe);
        assertEquals("sky", redMe.getName());

        assertNotNull(colorplus);
        assertEquals("Shirt", colorplus.getName());

    }

    @Test
    void shouldGetManufactures() {

        // given
        ProductReporter productReporter = new ProductReporter(productList);

        // when
        Set<String> manufactures = productReporter.getUniqueManufacturerNames();

        // then
        assertNotNull(manufactures);
        assertEquals(7, manufactures.size());
        assertTrue(manufactures.contains("oppo"));
        assertTrue( manufactures.contains("raymonds"));
        assertTrue( manufactures.contains("samsung"));
        assertTrue( manufactures.contains("Apple"));
        assertTrue( manufactures.contains("colorplus"));
        assertTrue( manufactures.contains("Manning"));
        assertTrue( manufactures.contains("redme"));

    }
}
